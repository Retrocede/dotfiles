----------------------------
---  Init from LunarVim  ---
----------------------------
lvim.log.level = "warn"
lvim.format_on_save = false
lvim.colorscheme = "melange"
vim.opt.termguicolors = true

--------------------------------------------------------------------
---  keymappings [view all the defaults by pressing <leader>Lk]  ---
--------------------------------------------------------------------
lvim.leader = "space"
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
lvim.keys.normal_mode["<C-t>"] = ":ToggleTerm<cr>"
lvim.keys.normal_mode["<Esc>"] = ":noh<cr>"
lvim.keys.normal_mode["<S-l>"] = ":BufferLineCycleNext<cr>"
lvim.keys.normal_mode["<S-h>"] = ":BufferLineCyclePrev<cr>"

------------------------------
---  Hide inline messages  ---
------------------------------
lvim.lsp.diagnostics.virtual_text = false

------------------------------------
---  Config for Builtin Plugins  ---
------------------------------------
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = false

lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "c",
  "javascript",
  "json",
  "lua",
  "python",
  "typescript",
  "tsx",
  "css",
  "rust",
  "java",
  "yaml",
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enable = true

----------------------------
---  Additional Plugins  ---
----------------------------
lvim.plugins = {
  ---------------
  ---  Theme  ---
  ---------------
  { "savq/melange" },

  ----------------------------
  ---  Color Highlighting  ---
  ----------------------------
  { 'uga-rosa/ccc.nvim' },

  -------------------
  ---  Languages  ---
  -------------------
  { 'vim-crystal/vim-crystal' },
  { 'dcharbon/vim-flatbuffers' },

  --------------------
  ---  Text Align  ---
  --------------------
  { 'echasnovski/mini.nvim' },

  ---------------
  --- Testing ---
  ---------------
  { "nvim-neotest/neotest-plenary" },
  { "marilari88/neotest-vitest" },
  { "nvim-neotest/neotest",
    requires = {
      "nvim-lua/plenary.nvim",
      "nvim-treesitter/nvim-treesitter",
      "antoinemadec/FixCursorHold.nvim",
      "marilari88/neotest-vitest",
    },
  },

  --------------------------
  ---  Markdown Preview  ---
  --------------------------
  { "ellisonleao/glow.nvim" },

  -----------------------
  ---  Image Preview  ---
  -----------------------
  { "nvim-telescope/telescope-media-files.nvim" },

  ---------------
  ---  Tasks  ---
  ---------------
  { 'arnarg/todotxt.nvim',
    requires = { 'MunifTanjim/nui.nvim' },
  },

  ------------------
  ---  Org Mode  ---
  ------------------
  { 'nvim-orgmode/orgmode' },

  -----------------------
  ---  TODO Comments  ---
  -----------------------
  { "folke/todo-comments.nvim" },

  -------------
  ---  GIT  ---
  -------------
  { 'kdheepak/lazygit.nvim' },
}


-------------
---  GIT  ---
-------------
lvim.builtin.which_key.mappings["gg"] = { "<cmd>LazyGit<cr>", "LazyGit" }


-----------------------
---  TODO Comments  ---
-----------------------
require("todo-comments").setup()

------------------
---  Org Mode  ---
------------------
require('orgmode').setup_ts_grammar()

require('nvim-treesitter.configs').setup {
  -- If TS highlights are not enabled at all, or disabled via `disable` prop,
  -- highlighting will fallback to default Vim syntax highlighting
  highlight = {
    enable = true,
    -- Required for spellcheck, some LaTex highlights and
    -- code block highlights that do not have ts grammar
    additional_vim_regex_highlighting = {'org'},
  },
  ensure_installed = {'org'}, -- Or run :TSUpdate org
}

require('orgmode').setup({
  org_agenda_files = {'~/org/*', '~/org/**/*'},
  org_default_notes_file = '~/org/refile.org',
})

vim.opt.conceallevel = 2
vim.opt.concealcursor = 'nc'


---------------
---  Tasks  ---
---------------
require('todotxt-nvim').setup({
  todo_file = "~/.todo/todo.txt",
  sidebar = { width = 100, position = "right" },
  capture = {
    prompt = "> ",
    width = "75%",
    position = "50%",
    alternative_priority = {
      A = "now",
      B = "next",
      C = "today",
      D = "this week",
      E = "next week",
    },
  },
  highlights = {
    project = {
      fg = "magenta",
      bg = "NONE",
      style = "NONE",
    },
    context = {
      fg = "cyan",
      bg = "NONE",
      style = "NONE",
    },
    date = {
      fg = "NONE",
      bg = "NONE",
      style = "underline",
    },
    done_task = {
      fg = "gray",
      bg = "NONE",
      style = "NONE",
    },
    priorities = {
      A = {
        fg = "red",
        bg = "NONE",
        style = "bold",
      },
      B = {
        fg = "magenta",
        bg = "NONE",
        style = "bold",
      },
      C = {
        fg = "yellow",
        bg = "NONE",
        style = "bold",
      },
      D = {
        fg = "cyan",
        bg = "NONE",
        style = "bold",
      },
    },
  },
  keymap = {
    quit = "q",
    toggle_metadata = "m",
    delete_task = "dd",
    complete_task = "<space>",
    edit_task = "ee",
  },
})

-----------------------
---  Image Preview  ---
-----------------------
require('telescope').load_extension('media_files')

-----------------------
---  Neotest setup  ---
-----------------------
require("neotest").setup({
  icons = {
    child_indent = "│",
    child_prefix = "├",
    collapsed = "─",
    expanded = "╮",
    failed = "✖",
    final_child_indent = " ",
    final_child_prefix = "╰",
    non_collapsible = "─",
    passed = "✔",
    running = "▶",
    skipped = "ﰸ",
    unknown = "?",
    todo = "?"
  },
  adapters = {
    require("neotest-plenary"),
    require('neotest-vitest')({
      vitestCommand = "npm run test:unit --",
      vitestConfigFile = "vitest.config.ts",
    }),
  },
})
vim.cmd([[
command! NeotestSummary lua require("neotest").summary.toggle()
command! NeotestFile lua require("neotest").run.run(vim.fn.expand("%"))
command! Neotest lua require("neotest").run.run(vim.fn.getcwd())
command! NeotestNearest lua require("neotest").run.run()
command! NeotestDebug lua require("neotest").run.run({ strategy = "dap" })
command! NeotestAttach lua require("neotest").run.attach()
]])
lvim.builtin.which_key.mappings["t"] = {
  name = "Tests",
  n = { "<cmd>NeotestNearest<cr>", "Nearest" },
  f = { "<cmd>NeotestFile<cr>", "File" },
  a = { "<cmd>Neotest<cr>", "All" },
  o = { "<cmd>NeotestSummary<cr>", "Summary" },
  d = { "<cmd>NeotestDebug<cr>", "Debug" },
  v = { "<cmd>NeotestAttach<cr>", "Attach" },
}

------------------------
---  Neovide config  ---
------------------------
vim.cmd([[
  set guifont=FuraCode\ Nerd\ Font:16
  let g:neovide_transparency = 0.96
  let g:neovide_cursor_vfx_mode = "ripple"
  let g:neovide_floating_blur_amount_x = 2.0
  let g:neovide_floating_blur_amount_y = 2.0
]])

----------------------------
---  Color Highlighting  ---
----------------------------
require("ccc").setup({
  highlighter = {
    auto_enable = true
  },
})

--------------------
---  Text Align  ---
--------------------
require('mini.align').setup()
